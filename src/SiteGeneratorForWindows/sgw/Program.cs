﻿using HeyRed.MarkdownSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace sgw
{
    class Program
    {
        Program(string[] args)
        {
            switch (args[0])
            {
                // TODO: put command line options into it's own file
                case "c":
                    GenerateProject(args[1]);
                    break;
                case "b":
                    if (args.Length == 1) BuildProject();
                    else if (args.Length == 2) BuildProject(args[1]);
                    break;
                case "bw":
                    while (true)
                    {
                        try
                        {
                            Thread.Sleep(10);
                            if (args.Length == 1) BuildProject();
                            else if (args.Length == 2) BuildProject(args[1]);
                        }
                        catch { continue; }
                    }
                default:
                    // TODO: display list of available command line options
                    break;
            }
        }

        /// <summary>
        /// Generate directory structure for a project. 
        /// This should include all required files and folders.
        /// </summary>
        /// <param name="projectName">Name of project, which is used as name of root directory.</param>
        private void GenerateProject(string projectName)
        {
            // folder paths
            string projectFolder = Path.Combine(".", projectName);
            string binFolder = Path.Combine(projectFolder, "bin");
            string srcFolder = Path.Combine(projectFolder, "src");

            // files paths
            string layoutFilePath = Path.Combine(srcFolder, "layout.html");
            string indexFilePath = Path.Combine(srcFolder, "index.md");

            // create folders
            Directory.CreateDirectory(projectFolder);
            Directory.CreateDirectory(binFolder);
            Directory.CreateDirectory(srcFolder);

            // create files
            FileStream layoutFile = File.Create(layoutFilePath);
            layoutFile.Close();

            FileStream indexFile = File.Create(indexFilePath);
            indexFile.Close();

            // write content to files
            #region html file content string
            // TODO: put this string somewhere else
            string html =
@"<!DOCTYPE html>
<html>
<head>
    <meta charset='UTF-8' />
    <style>
        * {
            font-family: Verdana, Geneva, sans-serif;
        }
    </style>
</head>
<body>
    <!-- CONTENT -->
</body>
</html>
";
            #endregion
            File.WriteAllText(layoutFilePath, html);

            string index = @"# Home page";
            File.WriteAllText(indexFilePath, index);
        }

        /// <summary>
        /// Compile project to static html website.
        /// </summary>
        public void BuildProject(string projectName = "")
        {
            // get all files and clean up build folder
            string[] files;
            DirectoryInfo dirInfo = null;
            switch (projectName)
            {
                case "":
                    // get files
                    files = Directory.GetFiles(Path.Combine(".", "src"), ".", SearchOption.AllDirectories);
                    dirInfo = new DirectoryInfo(Path.Combine(".", "bin"));
                    break;
                default:
                    files = Directory.GetFiles(Path.Combine(".", projectName, "src"), ".", SearchOption.AllDirectories);
                    dirInfo = new DirectoryInfo(Path.Combine(".", projectName, "bin"));
                    break;
            }

            // read contents of all files
            var htmlMds = new List<HtmlMd>();
            string layoutText = "<!-- CONTENT -->";
            foreach (string file in files)
            {
                if (Path.GetFileNameWithoutExtension(file) == "layout")
                {
                    layoutText = File.ReadAllText(file);
                }
                else
                {
                    var hm = new HtmlMd();
                    hm.BuildPath = file;
                    switch (Path.GetExtension(file).ToLower())
                    {
                        case ".md":
                            hm.Md = File.ReadAllText(file);
                            break;
                        case ".html":
                            hm.Html = File.ReadAllText(file);
                            break;
                    }
                    htmlMds.Add(hm);
                }
            }

            // clean up build folder
            try
            {
                foreach (var file in dirInfo.GetFiles()) file.Delete();
                foreach (var dir in dirInfo.GetDirectories()) dir.Delete(true);
            }
            catch
            {
                Console.WriteLine("Invalid build path.\n\nPress any key to quit...");
                Console.ReadKey();
            }

            // recreate directory structure in /bin
            string binDir;
            switch (projectName)
            {
                case "":
                    binDir = Path.Combine(".", "bin");
                    break;
                default:
                    binDir = Path.Combine(".", projectName, "bin"); ;
                    break;
            }
            Markdown md = new Markdown();
            foreach (var hm in htmlMds)
            {
                hm.Html = md.Transform(hm.Md);
                if (!string.IsNullOrWhiteSpace(hm.BuildPath))
                {
                    (new FileInfo(hm.BuildPath)).Directory.Create();
                    string textToWrite = layoutText.Replace("<!-- CONTENT -->", hm.Html);
                    File.WriteAllText(Path.ChangeExtension(hm.BuildPath, ".html"), textToWrite);
                }
            }
        }

        static void Main(string[] args)
        {
            new Program(args);
        }
    }
}
