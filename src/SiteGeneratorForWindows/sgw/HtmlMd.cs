﻿namespace sgw
{
    public class HtmlMd
    {
        private string _BuildPath;

        public string BuildPath { get { return _BuildPath; } set { _BuildPath = value.Replace("src", "bin"); } }
        public string Html { get; set; }
        public string Md { get; set; }
    }
}
